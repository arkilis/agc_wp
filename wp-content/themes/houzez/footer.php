<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content.
 *
 * @package Houzez
 * @since Houzez 1.0
 */
$copy_rights = houzez_option('copy_rights');
global $houzez_local;
?>

<?php if ( houzez_is_footer() ) { ?>

    <?php if( houzez_container_needed() ) { ?>
    </div> <!--.container Start in header-->
    <?php } ?>
</div> <!--Start in header end #section-body-->

<?php get_template_part('template-parts/scroll-to-top'); ?>

<!--start footer section-->
<footer id="footer-section">
    
    <?php get_template_part('template-parts/footer'); ?>

    <div class="footer-bottom">

    	<div class="container">
            <div class="row">
                <?php if( !empty($copy_rights) ) { ?>
                <div class="col-md-12 col-sm-12">
                    <div class="footer-col">
                        <p id="flyingbits_footer_bottom"><?php echo ( $copy_rights ); ?></p>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

    </div><!-- End footer bottom -->

</footer>
<!--end footer section-->
<?php } else { // End splash template if ?>
    </div> <!--Start in header end #section-body-->
<?php } ?>

<?php wp_footer(); ?>

</body>
</html>
