<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'agc_property');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '08hqlG8');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aj2keRw(TP^xtO}.q9BY{OGxA)D}m<%o6WVKH1(WPJmR.d&sb8g=ij#*gzI7xUHE');
define('SECURE_AUTH_KEY',  'OKj0m]S<CQWfE/{,>.|[01Zyh48&K3N+[Wbx*U}jO#<j#J[d&al(@ mMtMV99/D%');
define('LOGGED_IN_KEY',    'o}u&pWfy%?_4e)dL<BD[R|x;1SUE0KaG28_MV]2r@=tP2R_&/@KufWcCS-(CXhg4');
define('NONCE_KEY',        'm*<?UPHUxi&0r!{*>$3MfmT_wh .>&:-AFO4i(GOCNiDLKd`GT]2y`E?1eD;te#=');
define('AUTH_SALT',        '[|CaDXX[,L!x_,l@QPCsRmg}q[4KiL|RPJth} EP:[G:v>SF*Lud*Ltm(2V?D8rR');
define('SECURE_AUTH_SALT', 'qi&!clDd(e>w_b5xqi1|):Re=gQ*rX vgRhx?*A@k8bLbL{472N9).5IhE*<60.[');
define('LOGGED_IN_SALT',   'rDO`}:2$,$PmLXNkd0&j*Du(21M]/?n_c5q!@i7z4R!qOJYCXtp$#m eY`LV}&N|');
define('NONCE_SALT',       'rBO@<>w8y$c-4PRrGu-mr5A]u`Ap<ZJ`_dg)S7ef,0t`~3.A],: !9HF7#Cn-xul');

define( ‘WP_POST_REVISIONS’, false );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
